import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

import { Container } from 'reactstrap';

import Previsio from "./Js/Previsio";

export default () => (
  <Container>
    <Previsio />
  </Container>
);
