import React, { Component } from 'react'

import { Card, CardTitle, CardText, Col, CardImg } from 'reactstrap';

import "../Css/ItemTemperatura.css";
export default class ItemTemperatura extends Component {
    render() {
        let WeatherImage;
        switch (this.props.Lista[this.props.Id].weather[0].main) {
            case "Clouds":
                WeatherImage = "http://openweathermap.org/img/wn/02d@2x.png";
                break;
            case "Clear":
                WeatherImage = "http://openweathermap.org/img/wn/01d@2x.png";
                break;
            default:
                WeatherImage = "http://openweathermap.org/img/wn/50d@2x.png";
                break;

        }
        return (
            <Col md="2">
                <Card>
                    <CardTitle className="Centrado">{this.props.Fecha.getDate().toString()}</CardTitle>
                    <CardImg src={WeatherImage} alt={this.props.Lista[this.props.Id].weather[0].main} />
                    <CardText className="TemperaturaMinima Centrado">{this.props.Lista[this.props.Id].main.temp_min}</CardText>
                    <CardText className="TemperaturaMaxima Centrado">{this.props.Lista[this.props.Id].main.temp_max}</CardText>
                </Card>
            </Col>
        )
    }
}
