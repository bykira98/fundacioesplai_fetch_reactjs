import React, { Component } from 'react'

import {Row} from "reactstrap";
import ItemTemperatura from "./ItemTemperatura";

export default class Previsio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio() {
        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState(data))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }
        const TiempoEntreFechas = 8;
        const MaxFechas = 5;
        let Fechas = [];
        for (let i = 0; i < MaxFechas; i++) {
            Fechas.push({ Id: (i * TiempoEntreFechas), Fecha: (new Date(this.state.list[i * TiempoEntreFechas].dt * 1000)) })
        }

        let ItemsTemperatura = Fechas.map(Items => <ItemTemperatura key={Items.Id} Fecha={Items.Fecha} Id={Items.Id} Lista={this.state.list} />)
        return (
            <Row>
                {ItemsTemperatura}
            </Row>
        );
    }
}